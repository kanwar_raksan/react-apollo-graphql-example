import React from 'react';
import  ReactDOM from 'react-dom';
import App from '../components/App/App.js'
import LoginPage from '../components/Login/LoginPage.js'
import RegisterPage from '../components/AuthenticationForms/RegisterPage.js'
import InsertTask from '../components/tasks/insertTask.js'
import RMDetails from '../components/rmDetails/RMDetails';
import SettingsEntity from "../components/settingsEntity/SettingsEntity";
import tasksGrid from "../components/tasksGrid/tasksGrid";
import { Router,Route,Switch, Redirect} from 'react-router-dom';
import AdminLayout from "../components/adminLayout/AdminLayout.js";

const PrivateRouter = ({ render: Component, ...rest }) => (
  <Route {...rest} render={props => (
    localStorage.getItem("token") ? (
      <Component {...props}/>
    ) : (
      <Redirect to={{
        pathname: '/',
        state: { from: props.location }
      }}/>
    )
  )}/>
);
const adminContext = "/admin";
const Routes = (props) => (

    <Switch props={props}>
        <Route exact  path='/register' render={() =>(<RegisterPage {...props} />)} />
        <Route exact  path='/' render={() =>(<LoginPage />)} />
        <AdminLayout>
            <PrivateRouter path="/home"  render={()=>(<App {...props} />)} />
            <PrivateRouter path="/addTask"  render={()=>(<InsertTask {...props} />)} />
            <PrivateRouter path="/rmDetails" render={()=>(<RMDetails {...props} />)}/>
            <PrivateRouter path="/settingsEntity" render={()=>(<SettingsEntity  />)}/>
            <PrivateRouter path="/tasksGrid" component={tasksGrid} />
        </AdminLayout>
    </Switch>
);

export default Routes;
