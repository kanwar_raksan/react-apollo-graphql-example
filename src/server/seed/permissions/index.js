import * as _ from "lodash";

function getAuthorities(userType, module, operation) {
    try {
        let permissionSetFunc = require(`./${module}`);
        let permissions = permissionSetFunc();
        return _.find(permissions, ["userType", userType])[operation];
    } catch (e) {
        console.log(`Failed to get permission of unknown module ${JSON.stringify(e, null, 4)}`);
        throw new Error(e);
    }
}

module.exports = { getAuthorities };
