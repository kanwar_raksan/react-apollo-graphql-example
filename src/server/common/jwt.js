import jwt from 'jsonwebtoken';
import Users from "../model/users.model";
//const jwt = require('jsonwebtoken');


function generateToken(payload, expiresIn) {
  let token = jwt.sign(payload, 'secret', { expiresIn: expiresIn });
  return token;
}

async function authenticateToken(token) {
  if(token){
    let tokenVerificationResult;
    try{
      tokenVerificationResult = await jwt.verify(token,'secret');
    }
    catch(err){
      if(err){
        return({success: false, message: 'token authentication failed', payload:null});
      }
    }
    return ({success: true, message: 'token authentication successful', payload:tokenVerificationResult});
  }
}

module.exports = {
  generateToken:generateToken,
  authenticateToken:authenticateToken
}
