'use strict';
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var TasksSchema = new Schema({
    taskName: String,
    isDelete: Boolean,
    userId:String

});

TasksSchema.statics.updateTaskById = function(id, updatedTodo){
  return this.findByIdAndUpdate(id,{$set:updatedTodo});
}

TasksSchema.statics.removeTaskById = function(id) {
  return this.findByIdAndRemove(id).exec();
}

module.exports = mongoose.model('Tasks', TasksSchema);
