const User = `
    type User {
        id: String!,
        email: String!,
        password: String!,
        token: String!
    }
    
    type loginuserRes {
        userId: Int,
        token: String
    }
    
    type Mutation {
        createUser(
            email: String!,
            password: String!,
            isAdmin: Boolean!,
        ): User
        
        loginUser(
            email: String!,
            password: String!
        ): loginuserRes
    }
`;

export default User;